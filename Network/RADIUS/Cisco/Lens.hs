{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes        #-}
module Network.RADIUS.Cisco.Lens
  ( macAddr
  , pppDisconnect
  , discCauseExt
  , remoteId
  , circuitId
  )
where

import Network.RADIUS.Cisco
import Network.RADIUS.Types

import Optics
import Optics.TH

import Data.ByteString (ByteString)
import Data.Text (Text, stripPrefix)
import Data.Text.Encoding (decodeUtf8With, encodeUtf8)
import Data.Text.Encoding.Error (lenientDecode)

import Network.RADIUS.Lens

macAddr :: Prism' VSA Text
macAddr = avPrism "client-mac-address"

pppDisconnect :: Prism' VSA Text
pppDisconnect = avPrism "ppp-disconncet"

discCauseExt :: Prism' VSA Text
discCauseExt = avPrism "disc-cause-ext"

circuitId :: Prism' VSA Text
circuitId = avPrism "circuit-id-tag"

remoteId :: Prism' VSA Text
remoteId = avPrism "remote-id-tag"

avPrism :: Text -> Prism' VSA Text
avPrism nam = _VSACisco % _CiscoAVPair % prism' build match
  where
    build what = nam <> "=" <> what
    match = stripPrefix (nam <> "=")
