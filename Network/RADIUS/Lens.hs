{-# LANGUAGE TemplateHaskell #-}
module Network.RADIUS.Lens
where

import Optics.TH

import Network.RADIUS.Types
import Network.RADIUS.Cisco


$(makeClassyFor "HasPacket" "_radiusPacket"
                [ ("packetType", "_packetType")
                , ("packetId", "_packetId")
                , ("packetAuthenticator", "_packetAuthenticator")
                , ("packetAttributes", "_packetAttributes")
                ] ''Packet)

$(makePrisms ''Attribute)
$(makePrisms ''VSA)
$(makePrisms ''IngressFiltersState)
$(makePrisms ''AcctStatusType)
$(makePrisms ''Authentic)
$(makePrisms ''TerminateCause)
$(makePrisms ''ServiceType)
$(makePrisms ''FramedProtocol)
$(makePrisms ''FramedRouting)
$(makePrisms ''FramedCompression)
$(makePrisms ''LoginService)
$(makePrisms ''TerminationAction)
$(makePrisms ''NASPortType)
-- $(makePrisms ''Cisco)
